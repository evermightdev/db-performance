import requests
import json
import config

url = config.apiUrl + "mongo-log"
#url = config.apiUrl + "mysql-log"
#url = config.apiUrl + "mysql-json-log"
headers = {"Content-Type":"application/json"}
payload = {
	
	
	
	"details" : {
		"cap" : 76,
		"calibration_exsulation" : 1556.6,
		"notification_settings" : [
			{
				"notification_type" : "high",
				"notification_description" : "Slight unknown notification (Trigger when excessive load on servers and hardwards)",
				"start_time" : "02/07/2020, 14:37:45",
				"end_time" : "2020-01-31"
			}
		],
		"ttm" : 99,
		"facility_management" : 32,
		"Beta" : 25.4,
		"Gamma" : 25.5,
		"temp" : 19,
		"Nesulation" : 6553.4,
		"current" : 0,
		"auxillary" : [
			{
				"juiz" : 23,
				"vmiz" : 3.184,
				"juax" : 25,
				"units" : [
					{
						"cap" : 75,
						"temp" : 25,
						"ttm" : 100,
						"rank" : 0,
						"v" : 3.184,
						"id" : "unit1"
					},
					{
						"cap" : 75,
						"temp" : 24,
						"ttm" : 100,
						"rank" : 1,
						"v" : 3.182,
						"id" : "unit2"
					},
					{
						"cap" : 76,
						"temp" : 23,
						"ttm" : 100,
						"rank" : 2,
						"v" : 3.182,
						"id" : "unit3"
					},
					{
						"cap" : 75,
						"temp" : 23,
						"ttm" : 100,
						"rank" : 3,
						"v" : 3.183,
						"id" : "unit4"
					},
					{
						"cap" : 76,
						"temp" : 0,
						"ttm" : 100,
						"rank" : 4,
						"v" : 3.184,
						"id" : "unit5"
					},
					{
						"cap" : 76,
						"temp" : 0,
						"ttm" : 100,
						"rank" : 5,
						"v" : 3.184,
						"id" : "unit6"
					},
					{
						"cap" : 75,
						"temp" : 0,
						"ttm" : 100,
						"rank" : 6,
						"v" : 3.184,
						"id" : "unit7"
					},
					{
						"cap" : 75,
						"temp" : 0,
						"ttm" : 95,
						"rank" : 7,
						"v" : 3.184,
						"id" : "unit8"
					}
				],
				"v" : 25.7,
				"gmax" : 3.184,
				"id" : "1"
			}
		],
		"v" : 25.7,
		"notifier" : {
			"highgrade" : 1,
			"mediumc" : 1,
			"tertiaryfiller" : 0
		}
	},
	
}


data = json.dumps(payload)

while True:
  r = requests.post(url, headers=headers , data=data )

