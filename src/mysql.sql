CREATE TABLE IF NOT EXISTS `log` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `details` text NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `log2` (
  `id` bigint(32) unsigned NOT NULL AUTO_INCREMENT,
  `details` json NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
