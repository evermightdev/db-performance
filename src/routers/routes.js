
const MongoClient = require('mongodb').MongoClient;
let mongo = null;
const db = (async ()=>{
  const db = await MongoClient.connect(process.env.MONGODB_URL);
  mongo = db.db(process.env.MONGODB_NAME);
})();


const MysqlClient = require('mysql');
const mysql = MysqlClient.createConnection({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASS,
  database: process.env.MYSQL_NAME
});


(async ()=>{
  await mysql.connect(async(err)=>{
    if(err)
    {
      console.log("------------MYSQL CONNECT ERROR----------------");
      console.log(err);
      process.exit(1);
    }
  });
})();


const express = require('express')
const MongoLog = require('./mongo-log')
const MysqlLog = require('./mysql-log')
const router = express.Router()

router.post('/mongo-log', (req,res)=>MongoLog.post(req,res,mongo))
router.post('/mysql-log', (req,res)=>MysqlLog.post(req,res,'string',mysql))
router.post('/mysql-json-log', (req,res)=>MysqlLog.post(req,res,'json',mysql))

module.exports = router
