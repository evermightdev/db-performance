
var routes = {
  post:async(req,res,coltype,dbo) => {
    try {
        const details = JSON.stringify(req.body.details);
        const tbl = coltype === 'json' ? 'log2' : 'log';
        const sql = "INSERT INTO "+tbl+" (details,create_date) VALUES (?,NOW())";
        await dbo.query(sql,[details], function (err, result) {
            if (err)
            {
              console.log("------------MYSQL INSERT ERROR----------------");
              console.log(err);
              process.exit(1);
            }
        });
        res.status(201).send({"success":1})
    } catch (error) {
        console.log(error);
        res.status(400).send(error)
    }

  },
}
module.exports = routes
