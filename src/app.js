const express = require('express')
const port = process.env.PORT
const routes = require('./routers/routes')
var cors = require('cors')

const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)

if(process.env.SSL_KEY && process.env.SSL_CRT) {
    const https = require('https')
    const fs = require('fs')
		const options = {
			key: fs.readFileSync(process.env.SSL_KEY),
			cert: fs.readFileSync(process.env.SSL_CRT)
		}

		var server = https.createServer(options, app)

		server.listen(port, () => {
			console.log("server starting on port : " + port)
		})
} else {
    app.listen(port, () => {
        console.log(`Server running on port ${port}`)
    })
}
