# Intro

This project contains scripts that help you test the write performance of several databases.  We currently have scripts for the following databases:

1. MongoDB version 3.6
2. MySQL 5.7

We will create a table that has three columns:

1. id: primary key auto increment
2. details: json data (in the case of mysql, we have a log2 table where details is json data type, and log table where details is a text data type)
3. create_date: time stamp


# Requirements

1. NodeJS to start the API
2. Python3 - this will submit HTTP requests the NodeJS API
3. Mongo
4. MySQL


# Instructions

1. Start up a mongodb instance.
2. Make a copy of env.sample as .env and fill in the credentials for Mongo. We're using a mongo passwordless connection. You can change this by modifying src/routers/route.js where the mongo db connection is made.
3. Start up a mysql instance.
4. Import the src/mysql.sql to your mysql instance.  This will set up two tables.
5. Update the .env file with the mysql database connection details.
6. Run `npm start` to start the API.
7. Make a copy of config.sample.py as config.py and update the API url if necessary.
8. In the performance.py, you have several options for which `url` end point to test.  Uncomment the one you want to test.
9. From terminal, run `python3 performance.py` to start submitting data to the NodeJS API
